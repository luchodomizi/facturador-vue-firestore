import firebase from "firebase/app";
import "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyA3YRIRkB7lBhJe8ZHsj1ljKTJqWJ5PcXw",
  authDomain: "invoice-app-f0aee.firebaseapp.com",
  projectId: "invoice-app-f0aee",
  storageBucket: "invoice-app-f0aee.appspot.com",
  messagingSenderId: "1084596769548",
  appId: "1:1084596769548:web:e50fc773b79e05f243ef29"
};

const firebaseApp = firebase.initializeApp(firebaseConfig);

export default firebaseApp.firestore();